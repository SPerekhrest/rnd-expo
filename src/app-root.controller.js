
// outsource dependencies
// import _ from 'lodash';
import { takeEvery, put, call, select, delay } from 'redux-saga/effects';

// local dependencies
import { prepareController } from './services/controller';

const initial = {
  initialized: false,   // app made all required preparation
};

export const controller = prepareController({
  initial,
  prefix: 'app',
  types: ['INITIALIZE', 'CLOSE'],
  * subscriber () {
    yield takeEvery(controller.TYPE.CLOSE, silence, closeExecutor);
    yield takeEvery(controller.TYPE.INITIALIZE, silence, initializeExecutor);
  },
});
export default controller;

function * initializeExecutor ({ type, payload }) {
  // TODO important things for app initialization
  console.log(`%c SAGA ${type}`, 'color: #FF6766; font-weight: bolder; font-size: 12px;'
    , '\n payload:', payload);
  // NOTE it should be async - emulate
  yield delay(5 * 1000);
  // NOTE initialized
  yield put(controller.action.updateCtrl({ initialized: true }));
}

function * closeExecutor ({ type, payload }) {
  // TODO important things for app closing
  console.log(`%c SAGA ${type}`, 'color: #FF6766; font-weight: bolder; font-size: 12px;'
    , '\n payload:', payload);
  // NOTE initialized
  yield put(controller.action.updateCtrl({ initialized: false }));
}

/* HELPERS */
export function * silence (...args) {
  try { yield call(...args); } catch (error) {
    const action = args[1];
    // NOTE show critical error
    console.warn(`%c Error at ${action.type}`, 'color: #FF6766; font-weight: bolder; font-size: 12px;'
      , '\n action:', action
      , '\n\n error:', error);
  }
}
