
// outsource dependencies
import createSagaMiddleware from 'redux-saga';
import {
  createStore, applyMiddleware, compose, combineReducers,
} from 'redux';

// local dependencies
import { reducer as controller, sagas as controllerSagas } from './services/controller';

// Build the middleware to run our Sagas
const sagaMiddleware = createSagaMiddleware();
// Create store outside of root to be able dispatch actions from anywhere!
export const store = createStore(
  combineReducers({
    controller,
    // NOTE it may be "redux" libraries
  }),
  compose(applyMiddleware(sagaMiddleware)),
);

// initialize saga
sagaMiddleware.run(controllerSagas);

// Export
export default store;
