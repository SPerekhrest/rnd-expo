
export const config = {
  DEBUG: true,
  apiPath: 'api',
  environment: 'local',
  serviceUrl: 'http://82.117.249.182:9192'
};

export default config;

if (config.DEBUG) {
  // NOTE hide YellowBox
  // eslint-disable-next-line no-console
  console.disableYellowBox = true;
  console.info(
    '%c CONFIG ',
    'background: #EC1B24; color: #000; font-weight: bolder; font-size: 30px;'
    , '\n environment:', 'local'
    , '\n config:', config
  );
}
