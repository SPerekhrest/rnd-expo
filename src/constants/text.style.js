
// outsource dependencies


// local dependencies
import { DEFAULT_FONT_STYLES } from './root.theme';

/* --------------------
        TEXT
 ---------------------- */
export const common = {
  ...DEFAULT_FONT_STYLES,
  fontSize: 14,
  letterSpacing: 0.44,
};

export const caption = {
  ...common,
  fontSize: 12,
};

export const small = {
  ...common,
  fontSize: 10,
};

export const bold = {
  ...common,
  // NOTE Not all fonts have a variant for each of the numeric values
  // enum('normal', 'bold', '100', '200', '300', '400', '500', '600', '700', '800', '900')
  fontWeight: 'bold',
};

/* --------------------
        TITLE
---------------------- */
export const h1 = {
  ...bold,
  fontSize: 24,
};

export const h2 = {
  ...bold,
  fontSize: 22,
  letterSpacing: 0.15,
};

export const h3 = {
  ...bold,
  fontSize: 20,
  letterSpacing: 0.15,
};

export const h4 = {
  ...common,
  fontSize: 18,
  fontWeight: '400',
  letterSpacing: 0.15,
};

export const h5 = {
  ...common,
  fontSize: 16,
  fontWeight: '400',
};

export const h6 = {
  ...common,
  fontWeight: '400',
};
