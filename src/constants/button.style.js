
// outsource dependencies


// local dependencies
import { COLOR, OFFSET } from './root.theme';

/* --------------------
        TEXT
 ---------------------- */
export const btnText = {
  fontStyle: 'normal',
  fontWeight: 'bold',
  fontSize: OFFSET.POINT * 4
};

export const common = {
  borderWidth: 2,
  display: 'flex',
  margin: OFFSET.POINT,
  flexDirection: 'row',
  borderRadius: OFFSET.POINT,
  paddingTop: OFFSET.VERTICAL,
  paddingRight: OFFSET.HORIZONTAL,
  paddingBottom: OFFSET.VERTICAL,
  paddingLeft: OFFSET.HORIZONTAL,
  marginBottom: OFFSET.VERTICAL,
  backgroundColor: COLOR.THEME_BG,
};

export const pill = {
  ...common,
  borderRadius: OFFSET.VERTICAL + btnText.fontSize,
};
