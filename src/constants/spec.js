
export const NEW_ID = 'new';

export const STATUS = {
  DISABLED: 'DISABLED',
  ENABLED: 'ENABLED',
  DRAFT: 'DRAFT',
};

export const GENDER = {
  ALL: 'ALL',
  MALE: 'MALE',
  FEMALE: 'FEMALE',
};
