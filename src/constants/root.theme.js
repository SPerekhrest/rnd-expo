
// outsource dependencies
import Color from 'color';

// local dependencies
/**
 * Provide color functionality
 * @see {https://www.npmjs.com/package/color}
 */
export const COLOR = {
  RED: Color('#F55454'),
  GREEN: Color('#61BD4F'),
  PURPLE: Color('#C376E0'),
  YELLOW: Color('#FFD83D'),
  ORANGE: Color('#FFAB4A'),
  LIGHT_GREY: Color('#F3F3F3'),
  GREY: Color('#B0B0B0'),
  DARK_GREY: Color('#7B7B7B'),
  WHITE: Color('#FFFFFF'),
  BLACK: Color('#000000'),
  BLUE: Color('#319FCB'),
  DARK_BLUE: Color('#156F93'),
  TRANSPARENT: 'transparent',
  // NOTE theme
  THEME_BG: Color('#FFFFFF'),
  THEME_FONT: Color('#000000'),
  THEME_COLOR: Color('#156F93'),
  THEME_INVERSE: Color('#FFFFFF'),
  THEME_LIGHT_COLOR: Color('#319FCB'),
};

const offset = 4;
export const OFFSET = {
  POINT: offset,
  VERTICAL: offset * 3,
  HORIZONTAL: offset * 4,
};

export const DEFAULT_FONT_STYLES = {
  // fontFamily: 'Best of the best font',
  color: COLOR.BLACK,
  fontStyle: 'normal',
  fontWeight: 'normal',
};
