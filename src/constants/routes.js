
// outsource dependencies
// import _ from 'lodash';

// local dependencies
import { NEW_ID } from './spec';
import { config } from './index';

export const SIGN_IN = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('sign-in');

export const FORGOT_PASSWORD = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('forgot-password');

export const SIGN_UP = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('sign-up');

export const TERM_AND_CONDITIONS = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('term-and-conditions');

export const MAIN = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('main');

export const PROFILE = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('profile');

export const PERSONAL_INFO = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('personal-info');

export const ACCOUNT_SETTINGS = (base => ({
  ROUTE: base,
  LINK: linkTo.bind({ routeName: base })
}))('account-settings');

/**
 * @description
 interface NavigationNavigateActionPayload {
    routeName: string;
    params?: NavigationParams;
    // The action to run inside the sub-router
    action?: NavigationNavigateAction;
    key?: string;
 }
 * @example SETTINGS.LINK({ some: 'urlParam' query: {some: 'queryParam'}})
 * @param { Object } options
 * @returns { Object }
 * @function linkTo
 * @private
 */
function linkTo (options) {
  const { params, action, key } = options || {};
  config.DEBUG && console.info(
    '%c linkTo => ( options ) '
    , 'background: yellow; color: #0747a6; font-weight: bolder; font-size: 12px;'
    , '\n default:', this
    , '\n options:', options
  );
  return {
    routeName: this.routeName,
    params: params || this.params,
    action: action || this.action,
    key: key || this.key,
  };
}
