
// outsource dependencies
import PropTypes from 'prop-types';
import React, { memo, useEffect, useMemo, useState } from 'react';
import { Button, StatusBar, StyleSheet, Text, View } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

// local dependencies
import appRootCtrl from './app-root.controller';
import NavigationService from './services/navigation';
import { useController } from './services/controller';
import { InitialLoader } from './components/preloader';
// const AppScreens = createAppContainer(
//   createSwitchNavigator({
//     PublicScreens,
//     PrivateScreens,
//     ...wizards,
//   })
// );
//
// class Initializer extends PureComponent {
//   render () {
//     const { health, initialized, onNavigationStateChange } = this.props;
//     if (!health) { return <Preloader type="MAINTENANCE" active />; }
//     if (!initialized) { return <Preloader active />; }
//     return <AppScreens ref={NavigationService.setupNavigator} onNavigationStateChange={onNavigationStateChange} />;
//   }
// }
// Initializer.propTypes = {
//   health: PropTypes.bool.isRequired,
//   initialized: PropTypes.bool.isRequired,
//   onNavigationStateChange: PropTypes.func.isRequired,
// };
// const Root = connect(
//   state => ({ ...state.app }),
//   dispatch => ({
//     onNavigationStateChange: (prevState, newState, action) => dispatch({
//       type: APP_TYPES.NAVIGATION_CHANGE,
//       prevState,
//       newState,
//       action
//     })
//   })
// )(Initializer);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});

export default memo(() => {
  // NOTE subscribe app appRootController
  const [{ initialized }, { initialize, close }, isControllerConnected] = useController(appRootCtrl);
  useEffect(() => initialize() && close, []);
  const ready = useMemo(() => isControllerConnected && initialized, [isControllerConnected, initialized]);


  // TODO remove
  const [active, setActive] = useState(false);

  return <View style={styles.container}>
    <InitialLoader active={!ready}>
      <Text> Yes indeed i override entry point </Text>
      <Text> Nice =) </Text>
      <Text>
        {' '}
        ACTIVE:
        { active ? 'Yes' : 'No' }
      </Text>
      <Button onPress={() => setActive(!active)} title="Toggle" />
    </InitialLoader>
  </View>;
});
