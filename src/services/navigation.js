
// outsource dependencies
import { NavigationActions } from 'react-navigation';

// local dependencies
// import * as ROUTES from '../constants/routes';

class Service {
  // static get ROUTES () {
  //     return ROUTES;
  // }

  static get navigator () {
    // eslint-disable-next-line
    return Service._navigator;
  }

  /**
     * Use this only with the top-level (root) navigator of your app
     * @see {https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html}
     */
  static setupNavigator (navigator) {
    // eslint-disable-next-line
    Service._navigator = navigator;
  }

  static navigate = (params) => {
    Service.navigator.dispatch(
      NavigationActions.navigate(params),
    );
  };

  static goBack = (key = null) => {
    Service.navigator.dispatch(
      NavigationActions.back({ key }),
    );
  };
}

export const NavigationService = Service;
export default Service;
