
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { FontAwesome } from '@expo/vector-icons';
// import Icon from 'react-native-vector-icons/FontAwesome5';
import { StyleSheet, Animated, Easing } from 'react-native';

// local dependencies
import { COLOR } from '../constants/root.theme';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  }
});

class AppSpinner extends PureComponent {
    spinValue = new Animated.Value(0);

    componentDidMount = () => this.spin();

    spin = () => {
      this.spinValue.setValue(0);
      Animated.timing(this.spinValue, {
        toValue: 1,
        easing: this.props.easing,
        duration: this.props.duration,
        useNativeDriver: true,
      }).start(this.spin);
    };

    get style () {
      let allStyles = [styles.container];
      const spin = this.spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
      });
        // NOTE setup spinner styles
      allStyles.push({ transform: [{ rotate: spin }] });
      // NOTE setup custom stylesheet
      if (this.props.style) {
        allStyles = allStyles.concat(this.props.style);
      }
      return StyleSheet.flatten(allStyles);
    }

    render () {
      const { style, color, ...attr } = this.props;
      // NOTE propTypes of react-native-vector-icons/FontAwesome is incorrect
      // and does not known about react-native Color('#00000') functionality
      return <Animated.View style={this.style}>
        <FontAwesome {...attr} color={String(color)} />
      </Animated.View>;
    }
}
AppSpinner.propTypes = {
  size: PropTypes.number,
  name: PropTypes.string,
  easing: PropTypes.func,
  duration: PropTypes.number,
  color: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};
AppSpinner.defaultProps = {
  size: 18,
  style: null,
  // name: 'cog',
  // name: 'sun',
  // name: 'sync',
  // name: 'atom',
  // name: 'spinner',
  // name: 'asterisk',
  // name: 'sync-alt',
  name: 'yin-yang',
  // name: 'life-ring',
  // name: 'snowflake',
  // name: 'crosshairs',
  // name: 'certificate',
  // name: 'stroopwafel',
  // name: 'circle-notch',
  // name: 'compact-disc',
  // name: 'dharmachakra',
  duration: 10000,
  easing: Easing.linear,
  color: COLOR.THEME_FONT,
};

export default AppSpinner;
