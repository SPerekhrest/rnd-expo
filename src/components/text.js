
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, Text as UIText } from 'react-native';

// local dependencies
import { COLOR } from '../constants/root.theme';
import * as textStyles from '../constants/text.style';

// configure
const allowedAlign = ['auto', 'left', 'right', 'center', 'justify'];

export class Text extends PureComponent {
  get style () {
    let allStyles = [];
    const { variant, textAlign, color, style } = this.props;
    // NOTE setup base style based on text variant
    allStyles.push(textStyles[variant] || textStyles.common);
    // NOTE setup align
    if (allowedAlign.indexOf(textAlign) > -1) {
      allStyles.push({ textAlign });
    }
    // NOTE setup color
    color && allStyles.push({ color });
    // NOTE setup custom stylesheet
    if (style) {
      allStyles = allStyles.concat(style);
    }
    return StyleSheet.flatten(allStyles);
  }

  render () {
    const { children, textAlign, style, color, variant, ...attr } = this.props;
    return <UIText style={this.style} {...attr}>
      { children }
    </UIText>;
  }
}
Text.propTypes = {
  children: PropTypes.node,
  textAlign: PropTypes.oneOf(allowedAlign),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  color: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  variant: PropTypes.oneOf(Object.keys(textStyles).map(key => key)),
};
Text.defaultProps = {
  style: null,
  children: null,
  textAlign: null,
  variant: 'common',
  color: COLOR.THEME_FONT,
};

export default Text;
