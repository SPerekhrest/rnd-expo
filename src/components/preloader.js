
// outsource dependencies
import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';

// local dependencies
import Text from './text';
import AppSpinner from './app-spinner';
import { COLOR } from '../constants/root.theme';

/**
 * HOC contain logic to hide elements which wrapped by preloader
 */
export const withLoader = fn => ({ active = false, children = null, ...attr }) => (!active ? children : fn(attr));
// NOTE common propTypes for wrapped components
withLoader.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.element, PropTypes.node])
};

const MStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spinnerRow: {
    display: 'flex',
    marginBottom: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sp1: {
    alignSelf: 'flex-start'
  },
  sp2: {
    margin: -10
  },
  sp3: {
    alignSelf: 'flex-end'
  },
});
export const InitialLoader = withLoader(() => <View style={MStyles.container}>
  <View style={MStyles.spinnerRow}>
    <AppSpinner duration={1200} name="cog" size={70} color={COLOR.DARK_BLUE} style={MStyles.sp1} />
    <AppSpinner duration={2800} name="cog" size={150} color={COLOR.PURPLE} style={MStyles.sp2} />
    <AppSpinner duration={1600} name="cog" size={70} color={COLOR.GREEN} style={MStyles.sp3} />
  </View>
  <Text variant="h2" textAlign="center"> Hi ! </Text>
  <Text variant="h4" textAlign="center"> There =) </Text>
</View>);
