
# Expo seating chart application

#### Clone and run

Clone this repository. Run the following command in a Command Prompt or shell:
```
> npm install -g expo-cli
```

install dependencies:
```
> npm install
```

To start development process on android:
```
> npm run start
```

#### Debug

It`s also highly recommended to use [React-native-debugger](https://github.com/jhen0409/react-native-debugger) as a debugger. You can also check out the [Expo debugging documentation](https://docs.expo.io/versions/latest/workflow/debugging/).

#### Also useful [Expo documentation](https://docs.expo.io/versions/latest/)

 - [Installation](https://docs.expo.io/versions/v36.0.0/get-started/installation/)
 - [Commands](https://docs.expo.io/versions/latest/workflow/expo-cli/#commands)
 - [Glossary of terms](https://docs.expo.io/versions/v36.0.0/workflow/glossary-of-terms/)


#### Setup custom configuration *`... under implementation`*
