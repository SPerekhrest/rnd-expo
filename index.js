
// outsource dependencies
import React, { memo } from 'react';
import { Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import { registerRootComponent } from 'expo';

// local dependencies
import App from './src';
import store from './src/store';

export default registerRootComponent(memo(() => (
  <Provider store={store}>
    <StatusBar hidden />
    <App />
  </Provider>
)));
